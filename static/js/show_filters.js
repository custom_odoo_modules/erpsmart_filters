odoo.define('erpsmart_filters.show_filters', function (require) {
    var PublicWidget = require('web.public.widget');
    var ShowFilters = PublicWidget.Widget.extend({
        selector: '.show_filters',
        start: function () {
            const button = this.el;
            const container = button.closest('.products_attributes_filters')
            if(container){
                button.addEventListener('click', () => {
                    let filters = container.querySelectorAll('li.nav-item:not(.active)')
                    if(button.hasAttribute('open')){
                        button.innerHTML = 'Показать фильтры'
                        button.removeAttribute('open')
                        filters.forEach(filter => {
                            filter.classList.add('d-none')
                        });
                    }
                    else{
                        button.innerHTML = 'Скрыть'
                        button.setAttribute('open', 'true')
                        filters.forEach(filter => {
                            filter.classList.remove('d-none')
                        });
                    }
                })
            }
        },
    });
    PublicWidget.registry.show_filters = ShowFilters;
    return ShowFilters;
});