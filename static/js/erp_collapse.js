odoo.define('erpsmart_filters.erp_collapse', function (require) {
    var PublicWidget = require('web.public.widget');
    var ERPCollapse = PublicWidget.Widget.extend({
        selector: '.erp_collapse',
        start: function () {
            const container = this.el;
            let toggler = container.querySelector('[data-toggle]')
            let collapse = container.querySelector('[data-collapse]')
            if(toggler && collapse){
                toggler.addEventListener('click', () => {
                    let state = collapse.getAttribute('data-collapse')
                    if(state === 'false'){
                        collapse.style.height = collapse.scrollHeight + 'px'
                        collapse.setAttribute('data-collapse', 'true')
                        toggler.setAttribute('data-toggle', 'true')
                    }
                    else{
                        collapse.style.height = '0px'
                        collapse.setAttribute('data-collapse', 'false')
                        toggler.setAttribute('data-toggle', 'false')
                    }
                })
            }
        },
    });
    PublicWidget.registry.erp_collapse = ERPCollapse;
    return ERPCollapse;
});