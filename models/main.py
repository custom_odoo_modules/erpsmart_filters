from odoo import models, fields, api
from odoo import api, fields, models, _, Command

class AlwaysActiveFilters(models.Model):
    _inherit = "product.attribute"
    always_active = fields.Boolean(string="Всегда показывать", default=False, required=False)