# -*- coding: utf-8 -*-
{
    'name': "ERPSmart фильтры",

    'summary': """
        ERPSmart фильтры""",

    'author': "Xorikita",
    'website': "https://erpsmart.ru",
    'sequence': -2,
    'category': 'Website',
    'version': '15.0.1.0.0',

    'depends': ['website_sale'],

    'images': [
      'static/description/icon.png',
    ],

    'data': [
        "views/views.xml",
        "views/filters.xml",
    ],
    'assets': {
        'web.assets_frontend': [
            ('prepend', 'erpsmart_filters/static/scss/filters.scss'),
            'erpsmart_filters/static/js/erp_collapse.js',
            'erpsmart_filters/static/js/show_filters.js'
        ],
    },
    'licence': 'LGPL-3'
}
